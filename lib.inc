section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ;запись кода системного вызова
    syscall ;system call

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; Принимает: rdi
; Возвращает: rax
string_length:
    xor rax, rax
    .count:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .count
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
	ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rdx, rdx
    mov rax, rdi                ; теперь пишем в rax число для деления
    mov rcx, 10
    mov r9, rsp                ; сейвим положение стека
    dec rsp                    ; в стек пишем байт нуль-терминатор
    mov byte[rsp], dl
    mov r8, 10
    .loop:
        div r8
        add rdx, 0x30         ; цифру -> ASCII символ
        dec rsp                 ; цифра -> стек
        mov byte[rsp], dl
        xor rdx,rdx
        cmp rax,0
        ja .loop
    mov rdi,rsp                 ; вершина стека в качестве указателя на строку
    push r9                   ; сохраним старую вершину стека
    call print_string           ; отображение числа, как строки
    pop r9
    mov rsp,r9                ; стек в исходное положение
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx
    xor rbx, rbx
    xor rcx, rcx
.loop:
	mov al, byte[rdi+rcx]
	mov bl, byte[rsi+rcx]
	cmp al, bl
	jne .not_equal
	cmp al, 0
	je .equal
	inc rcx
	jmp .loop
.equal:
	mov rax, 1
	pop rbx
	ret
.not_equal:
	xor rax, rax
	pop rbx
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
	xor rdi, rdi
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    xor rbx, rbx
    mov r8, rdi
    mov r9, rsi
    jmp .loop

.check:
    or rbx, rbx
    jnz .word

.loop:
    call read_char
    or al, al
    jz .word

    cmp al, ' '
    je .check
    cmp al, `\n`
    je .check
    cmp al, `\t`
    je .check

    inc rbx
    cmp rbx, r9
    ja .error

    mov [r8+rbx-1], al
    jmp .loop

.word:
    mov byte [r8+rbx], 0
    mov rax, r8
    mov rdx, rbx
    pop rbx
    ret
.error:
    xor rax, rax
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
	xor rcx, rcx
	xor rsi, rsi
	mov r9, 10
	.loop:
		mov cl, byte[rdi+rsi]
		xor cl, 0x30
		cmp cl, 9
		ja .error
		mul r9
		add rax, rcx
		inc rsi
		jmp .loop
	.error:
		mov rdx, rsi
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx

    push rdi ;str pointer
    push rsi ;buf pointer
    push rdx ;buf len
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .large

    xor rdx, rdx
    .loop:
        mov cl, [rdi + rdx]
        mov [rsi + rdx], cl
        inc rdx
        cmp rdx, rax
        jle .loop
        ret
    .large:
        xor rax, rax
        ret

